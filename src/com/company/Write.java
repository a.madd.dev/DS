package com.company;

import javafx.scene.shape.Path;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.EnumSet;

public class Write {

    public void write(String FileName , String data) throws IOException {

        CharBuffer charBuffer = CharBuffer
                .wrap(data);

        File file = new File(FileName);

        //Delete the file; we will create a new file
        file.delete();

        // Get file channel in readonly mode
        FileChannel fileChannel = new RandomAccessFile(file, "rw").getChannel();

        // Get direct byte buffer access using channel.map() operation
        MappedByteBuffer buffer = fileChannel.map(FileChannel.MapMode.READ_WRITE, 0, charBuffer.length());

        //Write the content using put methods
        //  buffer.put("howtodoinjava.com".getBytes());


        if (buffer != null) {
            buffer.put(
                    Charset.forName("utf-8").encode(charBuffer));
        }


    }


}
